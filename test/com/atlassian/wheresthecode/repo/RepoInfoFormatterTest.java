package com.atlassian.wheresthecode.repo;

import com.atlassian.wheresthecode.handlebars.Templates;
import com.atlassian.wheresthecode.maven.MavenGav;
import com.atlassian.wheresthecode.maven.MavenProject;
import com.atlassian.wheresthecode.maven.MavenScmInfo;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.wheresthecode.repo.RepoInfo.RepoType.GIT;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class RepoInfoFormatterTest {

    private RepoInfoFormatter repoInfoFormatter;
    private MavenScmInfo mavenScmInfo;
    private MavenGav gav;
    private MavenGav parentGav;
    private MavenProject mavenProject;

    @Before
    public void setUp() throws Exception {
        repoInfoFormatter = new RepoInfoFormatter();
        mavenScmInfo = new MavenScmInfo(
                option("scm:git:ssh://git@stash.atlassian.com:7997/JIRA/servicedesk.git"),
                option("http://some.url.com")
        );
        gav = gav("com.atlassian.code", "artifact", "1.2.3");
        parentGav = gav("com.atlassian.code", "parent", "1.2.3");
        mavenProject = new MavenProject(
                gav,
                option(parentGav),
                option(mavenScmInfo)

        );
    }

    @Test
    public void testFormat() throws Exception {

        RepoInfo repoInfo = repoInfoFormatter.format(
                mavenProject,
                mavenScmInfo
        );

        assertThat(repoInfo.getAddress(), equalTo("git@stash.atlassian.com:7997/JIRA/servicedesk.git"));
        assertThat(repoInfo.getCloneCommand(), equalTo("git clone git@stash.atlassian.com:7997/JIRA/servicedesk.git"));
        assertThat(repoInfo.getUrl(), equalTo("http://some.url.com"));
        assertThat(repoInfo.getType(), equalTo(GIT));
        assertThat(repoInfo.getGav().getGroupId(), equalTo("com.atlassian.code"));
        assertThat(repoInfo.getGav().getArtifactId(), equalTo("artifact"));
        assertThat(repoInfo.getGav().getVersion(), equalTo("1.2.3"));
    }

    @Test
    public void testMissingGavAttributes() throws Exception {

        gav = gav(null, "artifact", null);
        parentGav = gav("com.atlassian.code", "parent", "1.2.4");
        mavenProject = new MavenProject(
                gav,
                option(parentGav),
                option(mavenScmInfo)

        );

        RepoInfo repoInfo = repoInfoFormatter.format(
                mavenProject,
                mavenScmInfo
        );

        assertThat(repoInfo.getGav().getGroupId(), equalTo("com.atlassian.code"));
        assertThat(repoInfo.getGav().getArtifactId(), equalTo("artifact"));
        assertThat(repoInfo.getGav().getVersion(), equalTo("1.2.4"));
    }

    @Test
    public void test_html_generation() throws Exception {
        String repoInfo = Templates.eval("repoInfo", repoInfoFormatter.format(
                mavenProject,
                mavenScmInfo
        ));

        assertThat(repoInfo.contains("git clone git@stash.atlassian.com:7997/JIRA/servicedesk.git"), equalTo(true));

    }

    private MavenGav gav(String groupId, String artefactId, String version) {
        return new MavenGav(option(groupId), artefactId, option(version));
    }
}